FROM haxe:4.1.4-buster
LABEL author="artur@barichello.me"

COPY build build
COPY docs/docs.hxml /

RUN apt-get update && apt-get install -y --no-install-recommends \
    g++ \
    libmbedtls-dev \
    libopenal-dev \
    libpng-dev \
    libsdl2-dev \
    libturbojpeg-dev \
    libuv1-dev \
    libvorbis-dev \
    make \
    && \
    haxelib install --always build/webgl.hxml && \
    haxelib install --always build/c-hashlink.hxml && \
    haxelib install --always build/hl-hashlink.hxml && \
    haxelib install --always build/server.hxml && \
    haxelib install --always docs.hxml && \
    haxelib install dox && \
    haxelib install formatter

RUN git clone https://github.com/HaxeFoundation/hashlink && \
    cd hashlink && make && make install
