package src.utils;

/** Custom extensions to Haxe's Math API **/
class MathExtensions {
    public static inline function randomInt(cl: Class<Math>, from: Int, to: Int): Int {
        return from + Math.floor(((to - from + 1) * Math.random()));
    }

    // @:generic
    public static inline function clamp(cl: Class<Math>, value: Int, min: Int, max: Int): Int {
        return value < min ? min : (value > max ? max : value);
    }
}

#if SERVER
import src.network.Peer;

class PeerArrayExtensions {
    public static inline function readyPeers(cl: Array<Peer>): Int {
        return cl.filter((peer) -> peer.ready).length;
    }
}
#end
