package src.utils;

@:generic
class Averager<T> {
    var list: List<T> = new List<T>();

    public var average: T;
    public var length: Int;

    public function new(startValue: T, length: Int) {
        this.length = length;
        this.average = startValue;
        list.add(startValue);
    }

    public function add(value: T) {
        list.add(value);
        if (list.length >= length) {
            list.pop();
        }
        average = calculateAverage();
    }

    public function calculateAverage(): Dynamic {
        var average: Dynamic = 0;
        for (el in list) {
            average += el;
        }
        return Std.int(average / list.length);
    }
}
