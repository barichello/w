package src.utils;

import haxe.Timer;

class Stopwatch {
    var ms = 0.0;
    var lastStamp = 0.0;

    public inline function new() {}

    public function start() {
        lastStamp = Timer.stamp();
    }

    public function stop(): Float {
        var now = Timer.stamp();
        return now - lastStamp;
    }

    public function restart(): Float {
        var ms = stop();
        start();
        return ms;
    }
}
