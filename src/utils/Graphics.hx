package src.utils;

class Graphics {
    public static function drawCell(g: h2d.Graphics, position: {x: Float, y: Float}, size: Float, color: Int) {
        g.beginFill(color);
        g.drawRect(position.x, position.y, size, size);
        g.endFill();
    }
}
