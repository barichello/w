package src.utils;

class FPS {
    var frameCount = 0;
    var updateRate = 2;
    var delta = 0.0;

    public var fps = 0.0;

    public function new() {}

    public function update(delta: Float) {
        frameCount++;
        this.delta += delta;
        if (this.delta > 1.0 / updateRate) {
            this.fps = frameCount / this.delta;
            frameCount = 0;
            this.delta -= 1.0 / updateRate;
        }
    }
}
