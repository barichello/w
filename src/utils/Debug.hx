package src.utils;

import src.network.Protocol.PacketHeader;

class Debug {
    public static function shouldLog(header: PacketHeader): Bool {
        return switch (header) {
            case Client(Ping) | Server(Pong) | Server(PeerUpdate):
                return false;
            default:
                return true;
        }
    }
}
