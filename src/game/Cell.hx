package src.game;

#if CLIENT
import src.utils.Graphics;
#end

enum State {
    Dead;
    Alive;
}

class Cell {
    public var state = Dead;
    public var position = {x: 0.0, y: 0.0};

    var color: Int = deadColor;

    // todo: move color to state enum constructor
    public static inline var deadColor = 0;
    public static inline var aliveColor = 0xBABABA;

    public static inline var SIZE = 32;

    public function new(x = 0.0, y = 0.0) {
        setPosition(x, y);
    }

    public function setPosition(x: Float, y: Float) {
        position.x = x;
        position.y = y;
    }

    #if CLIENT
    public function update() {
        this.color = deadColor;
    }

    public function setState(state: State) {
        this.state = state;
        color = switch (state) {
            case Dead:
                deadColor;
            case Alive:
                aliveColor;
        }
    }

    public function highlight() {
        // todo: temp
        color = 0xCCCCCC;
    }

    public function draw(g: h2d.Graphics) {
        Graphics.drawCell(g, position, SIZE, color);
    }
    #end
}
