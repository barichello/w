package src.game;

import src.game.Cell;
import src.utils.Extensions.MathExtensions;

using src.utils.Extensions.MathExtensions;

typedef BoardMatrix = Array<Array<Cell>>;

class Board {
    public static inline var SIZE: Int = 32;

    public static var board: BoardMatrix = [for (x in 0...SIZE) [for (y in 0...SIZE)
        new Cell(x * (SIZE), y * (SIZE))]];

    public function new() {
        #if CLIENT
        hxd.Window.getInstance().addEventTarget(onEvent);
        #end
    }

    #if CLIENT
    public function update() {
        for (line in board) {
            for (cell in line) {
                cell.update();
            }
        }
    }

    public function onEvent(event: hxd.Event) {
        switch (event.kind) {
            case EMove:
                highlightCell(event.relX, event.relY);
            case _:
        }
    }

    function highlightCell(x: Float, y: Float) {
        var cellX = Math.clamp(Std.int(x / SIZE), 0, SIZE - 1);
        var cellY = Math.clamp(Std.int(y / SIZE), 0, SIZE - 1);
        board[cellX][cellY].highlight();
    }

    public function draw(g: h2d.Graphics) {
        for (i in 0...board.length) {
            for (j in 0...board.length) {
                board[i][j].draw(g);
            }
        }
    }
    #elseif SERVER
    public function nextGeneration() {}
    #end
}
