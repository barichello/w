package src.game;

import src.network.Client;

class Game extends hxd.App {
    @:nullSafety(Off)
    var g: h2d.Graphics;
    @:nullSafety(Off)
    var debugText: h2d.Text;
    @:nullSafety(Off)
    var client: Client;

    override function init() {
        g = new h2d.Graphics(s2d);
        debugText = new h2d.Text(hxd.res.DefaultFont.get(), s2d);
        debugText.textAlign = Left;

        client = new Client(s2d);
    }

    override function update(dt: Float) {
        debugText.text = makeDebugText();
        input();
        draw();
        client.board.update();
    }

    public function input() {
        client.player.input();
    }

    public function draw() {
        g.clear();
        client.draw(g);
    }

    function makeDebugText(): String {
        return '\nPing: ${client.pings.average} ms\n\n';
    }
}
