package src.game;

import haxe.Int32;
import haxe.ds.Option;
import hxd.Key;
import src.utils.Graphics;

class Player {
    public function new() {}

    public function input() {
        if (Key.isDown(Key.W)) {}
        if (Key.isDown(Key.S)) {}
        if (Key.isDown(Key.A)) {}
        if (Key.isDown(Key.D)) {}
    }

    public function draw(g: h2d.Graphics) {}
}
