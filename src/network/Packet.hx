package src.network;

import haxe.Int32;
import haxe.io.BytesBuffer;
import haxe.io.Bytes;
import haxe.ds.Option;
import src.network.Protocol;
import src.network.Protocol.PacketContent;
import src.network.Protocol.PacketHeader;
import src.network.Protocol.PacketType;
import src.utils.Debug;

// import src.utils.Assert;

/**
    Extension of BytesBuffer with utility methods for serializing packets
    according to src.network.Protocol
**/
class Packet extends BytesBuffer {
    public var header: PacketHeader;

    // TODO: replace option<> with optional arg
    public function new(packetHeader: PacketHeader, packetContents: Option<Array<PacketContent>> = None) {
        super();
        this.header = packetHeader;

        addInt32(Packet.headerToInt32(packetHeader));

        switch (packetContents) {
            case None:
                return;

            case Some(h):
                for (content in h) {
                    switch (content) {
                        case Int(i):
                            addInt32(i);
                        case Float(f):
                            addDouble(f);
                        case _:
                            trace('Received unknown packet content: ${content}');
                    }
                }
        }
    }

    /**
        Get all elements from packet (excluding packetHeader)
        @param bytes packet bytes
        @param header PacketHeader enum
        @return list of PacketContents
    **/
    public static function get(bytes: Bytes, header: PacketHeader): Array<PacketContent> {
        var structure = Protocol.packetContentMap.get(header);
        if (structure != null) {
            return deserializePacket(bytes, structure)[0];
        }
        trace("Unknown structure");
        return [];
    }

    /**
        Get all elements from packet (excluding packetHeader)
        @param bytes packet bytes
        @param structure SizeOfPacketContents representing the structure to be deserialized N times
        @param times number of structures
        @return list of PacketContents
    **/
    public static function getMultiple(bytes: Bytes, structure: SizeOfPacketContents, times: Int): Array<Array<PacketContent>> {
        var offset = 8; // Skip the int32 used to represent 'times'
        return deserializePacket(bytes, structure, times, 8);
    }

    /** Common function used between get and getMultiple **/
    private static function deserializePacket(bytes: Bytes, structure: SizeOfPacketContents, times: Int = 1, offset: Int = 4): Array<Array<PacketContent>> {
        var result = new Array<Array<PacketContent>>();

        for (_ in 0...times) {
            var structArray = [];
            for (sizeof in structure) {
                if (sizeof == Protocol.int) {
                    var int = bytes.getInt32(offset);
                    structArray.push(Int(int));
                } else if (sizeof == Protocol.float) {
                    var float = bytes.getDouble(offset);
                    structArray.push(Float(float));
                }
                offset += sizeof;
            }
            result.push(structArray);
        }
        // Assert.assert(result.length == times);
        return result;
    }

    public static function headerToInt32(packetHeader: PacketHeader): Int32 {
        return switch (packetHeader) {
            case Server(header): header.getIndex();
            case Client(header): header.getIndex();
        }
    }

    public static function int32ToHeader(headerIndex: Int32, type: PacketType): PacketHeader {
        return switch (type) {
            case Server: PacketHeader.Server(ServerHeader.createByIndex(headerIndex));
            case Client: PacketHeader.Client(ClientHeader.createByIndex(headerIndex));
        }
    }

    /**
        @param packet bytes extracted from src.network.Packet
        @param type client or server packet
        @return packet header enum
    **/
    public static function extractPacketHeader(bytes: Bytes, type: PacketType) {
        var packetHeaderIndex = bytes.getInt32(0);
        var packetHeader = int32ToHeader(packetHeaderIndex, type);
        #if debug
        if (Debug.shouldLog(packetHeader)) {
            trace('\n\t-> Packet received\n\tPacket header: ${packetHeader}\n\tHex: ${bytes.toHex()}');
        }
        #end
        return packetHeader;
    }
}
