/** Common file between game and server **/

package src.network;

import haxe.io.Bytes;
import haxe.Int32;
import src.network.Packet;

/** Origin of the packet **/
enum PacketType {
    Server;
    Client;
}

enum PacketHeader {
    Server(header: ServerHeader);
    Client(header: ClientHeader);
}

/** Types which can be sent through the socket **/
enum SendType {
    PacketMsg(p: Packet);
    StrMsg(s: String);
}

/** All valid types that can be packed in a Packet **/
enum PacketContent {
    /** 32 bits **/
    Int(i: haxe.Int32);

    /** 64 bits **/
    Float(f: Float);

    /** Size is the rest of the packet **/
    Bytes(b: Bytes);
}

/** Client Headers from client-created packets - value is provided by enum index **/
enum ClientHeader {
    /** Client ping packet timestamp **/
    Ping;

    /** Client connection request **/
    Connect;
}

/** Server Headers from server-created packets - value is provided by enum index **/
enum ServerHeader {
    /** Used as a response to a Client(Ping) packet with timestamp of client-sent packet **/
    Pong;

    /** Used to disconnect the peer and send a message **/
    ErrorDisconnect;

    /** Used to give the Client his ID and initial position after a successfull connection **/
    SpawnPlayer;

    /** Contains the number of players and their update info **/
    PeerUpdate;
}

/** Array of results of sizeofPacketContent function **/
typedef SizeOfPacketContents = Array<Int>;

/** Static variables necessary for Protocol utilities **/
class Protocol {
    public static inline var tickrate = 90; // Hz
    public static inline var tickInterval = Std.int(1000 / tickrate); // ms
    public static inline var debugInterval = 1000; // ms

    /** Static sizeof PacketContent.Int **/
    public static inline var int = 4;

    /** Static sizeof PacketContent.Float **/
    public static inline var float = 8;

    /** Static sizeof PacketContent.Bytes **/
    public static inline var variable = 0;

    /**
        Map between packet type and the size of packet contents in bytes, useful to extract variables
        based only on position (this excludes packet header which is present in all packets)
    **/
    public static var packetContentMap: Map<PacketHeader, SizeOfPacketContents> = [
        Client(Ping) => [float], // timestamp
        //
        //
        //
        Server(Pong) => [float], // timestamp
    ];

    /** Structures that can be repeated multiple times in a Packet **/
    // todo

    /** --- **/
    //
    // Packet headers for string messages (3 length strings)
    #if debug
    public static inline var DEBUGINFO = "INF";
    #end
    public static inline var DISCONNECT_ERROR = "DSC";
}
