package src.network;

import haxe.Timer;
import haxe.Int32;
import haxe.io.Bytes;
import haxe.ds.Option;
import hx.ws.SocketImpl;
import hx.ws.WebSocketHandler;
import hx.ws.Types;
import src.network.World;
import src.network.Packet;
import src.network.Protocol;
import src.network.Protocol.PacketType;
import src.utils.Extensions.MathExtensions;
import src.utils.Extensions.PeerArrayExtensions;

using src.utils.Extensions.PeerArrayExtensions;
using src.utils.Extensions.MathExtensions;

/** Represents a peer from the server's view **/
class Peer extends WebSocketHandler {
    public var socket: SocketImpl;
    public var peerId: Int32 = Math.randomInt(0, 999); // todo: handle id collision
    public var ready = false;
    public var timedout = false;
    public var lastPacket: Float = Timer.stamp();

    var peers = [];
    var world = new World();

    /**
        @param s socket created in handler
    **/
    public function new(s: SocketImpl) {
        super(s);
        this.socket = s;
    }

    public function setupPeer(world: World) {
        onopen = onOpen;
        onerror = onError;
        onmessage = onMessage;
        this.world = world;
    }

    public function update(peers: Array<Peer>) {
        this.peers = peers;
    }

    function onOpen() {}

    function onError(error) {
        trace('ERROR: ${error}');
    }

    function onMessage(message: MessageType) {
        switch (message) {
            case BytesMessage(b):
                bytesMessage(b.readAllAvailableBytes());

            case StrMessage(str):
                stringMessage(str);

            default:
                trace('Received unknown message:${message}');
        }
    }

    // --- packet processing ---

    public function sendToClient(msg: SendType) {
        switch (msg) {
            case PacketMsg(p):
                send(p.getBytes());
            case StrMsg(s):
                send(s);
        }
    }

    function stringMessage(str: String) {
        trace('Incoming message: ${str}');
    }

    function bytesMessage(bytes: Bytes) {
        var packetHeader: PacketHeader = Packet.extractPacketHeader(bytes, PacketType.Client);
        lastPacket = Timer.stamp();

        switch (packetHeader) {
            case Client(Ping):
                processClientPing(bytes);
            case Client(Connect):
                processClientConnect(bytes);
            case _:
                trace('Received unexpected packet header: ${packetHeader}');
        }
    }

    /** Process Client(Ping) **/
    function processClientPing(bytes: Bytes) {
        var sentPingStamp: Float = Packet.get(bytes, Client(Ping))[0].getParameters()[0];
        var contents = [Float(sentPingStamp)];
        var packet = new Packet(Server(Pong), Some(contents));
        sendToClient(PacketMsg(packet));
    }

    /** Process Client(Connect) **/
    function processClientConnect(bytes: Bytes) {}
}
