package src.network;

import haxe.ds.Option;
import src.game.Board;

/** World static data **/
class World {
    public var Board = new Board();

    public function new() {}

    public function update(delta: Float, peers: Array<Peer>) {}
}
