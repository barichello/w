package src.network;

import haxe.Timer;
import hx.ws.Log;
import hx.ws.WebSocketServer;
import src.network.Protocol;
import src.network.Peer;
import src.network.World;
import src.network.Packet;
import src.utils.Utils;
import src.utils.Stopwatch;

/** WebsocketServer initialization and world creation **/
class Server {
    var server: WebSocketServer<Peer>;
    var timeoutKicker = new Timer(TIMEOUT * 1000);

    static inline var TIMEOUT = 10;

    var port = 9001;
    var maxConnections = 10;
    var debug = Utils.Debug();

    static var peers: Array<Peer> = [];
    static var world = new World();

    var tickStopwatch = new Stopwatch();
    var tickTimer = new haxe.Timer(Protocol.tickInterval);
    var debugTimer = new haxe.Timer(Protocol.debugInterval);

    public function new() {
        #if SOCKET_LOG
        Log.mask = Log.INFO | Log.DEBUG | Log.DATA;
        #end
        server = new WebSocketServer<Peer>("localhost", port, maxConnections);
        server.onClientAdded = onPeerConnect;
        server.onClientRemoved = onPeerDisconnect;

        timeoutKicker.run = kickTimedoutPeers;
        tickTimer.run = tick;
        #if debug
        debugTimer.run = debugStep;
        #end

        var startStr = 'Started websocket server\nPort: ${port}\nMax connections: ${maxConnections}\n';
        startStr += 'Tickrate: ${Protocol.tickrate} Hz\nDebug: ${debug}\n---';
        trace(startStr);

        server.start();
    }

    public function onPeerConnect(peer: Peer) {
        peer.setupPeer(world);
        logCurrentPlayers();
    }

    public function onPeerDisconnect(peer: Peer) {
        logCurrentPlayers();
    }

    #if debug
    /** Send debug string packet to peers **/
    function debugStep() {
        if (peers.length > 0) {
            var msg = StrMsg('${Protocol.DEBUGINFO}|${debugStrInfo()}');
            for (p in peers) {
                p.sendToClient(msg);
            }
        }
    }
    #end

    function tick() {
        var delta = tickStopwatch.restart();
        peers = server.handlers;
        world.update(delta, peers);

        // var updateMsg = world.makePeerUpdatePacket(peers);
        // for (peer in peers) {
        //     if (!peer.ready) {
        //         continue;
        //     }

        //     peer.update(peers);
        //     switch (updateMsg) {
        //         case Some(p):
        //             peer.sendToClient(PacketMsg(p));
        //         case None:
        //     }
        // }
    }

    function kickTimedoutPeers() {
        for (peer in peers) {
            var now = Timer.stamp();
            if (peer.ready && now - peer.lastPacket > TIMEOUT) {
                trace('Peer ${peer.peerId} timedout');
                peers.remove(peer);
                peer.socket.close();
                logCurrentPlayers();
            }
        }
    }

    public inline function connectedPlayers(): Int {
        return server.totalHandlers();
    }

    public inline function logCurrentPlayers() {
        trace('Connected peers: ${connectedPlayers()}');
    }

    #if debug
    /** Debug string to be sent to peers **/
    public function debugStrInfo(): String {
        var s: String = 'Players: ${connectedPlayers()}\n\n';
        for (peer in peers) {
            // var state = peer.currentState();
            // s += 'id:${peer.peerId}, x: ${state.x}, y: ${state.y}\n';
        }
        return s;
    }
    #end
}
