package src.network;

import haxe.Timer;
import haxe.Int32;
import haxe.io.Bytes;
import haxe.ds.Option;
import hx.ws.WebSocket;
import hx.ws.Types;
import src.game.Board;
import src.game.Player;
import src.network.Protocol;
import src.network.Packet;
import src.utils.Averager;

using Lambda;

/** Client websocket interface **/
class Client {
    var ws = new WebSocket("ws://localhost:9001");
    var pinger = new Timer(500);

    public var player: Player = new Player();
    public var players: Map<Int32, Player> = [];
    public var board: Board = new Board();
    public var pings = new Averager<Int>(1, 10);

    #if debug
    public var debugText: h2d.Text;
    #end

    public function new(s2d: h2d.Scene) {
        #if debug
        debugText = new h2d.Text(hxd.res.DefaultFont.get(), s2d);
        debugText.textAlign = Left;
        debugText.x = 500;
        #end
        setupWebsocket();
    }

    function setupWebsocket() {
        #if SOCKET_LOG
        Log.mask = Log.INFO | Log.DEBUG | Log.DATA;
        #end
        ws.onopen = onOpen;
        ws.onerror = onError;
        ws.onmessage = onMessage;
        pinger.run = sendPing;
    }

    function onOpen() {
        trace("Started client websocket");
        sendToServer(new Packet(Client(Connect)));
    }

    function onError(error: Dynamic) {
        trace('ERROR: ${error}');
    }

    function onMessage(message: Dynamic): Void {
        switch (message) {
            case BytesMessage(b):
                bytesMessage(b.readAllAvailableBytes());
            case StrMessage(s):
                stringMessage(s);
        }
    }

    public function draw(g: h2d.Graphics) {
        board.draw(g);
    }

    // --- packet processing ---

    public function sendToServer(packet: Packet) {
        ws.send(packet.getBytes());
    }

    /** Send Client(Ping) packet to server **/
    public function sendPing() {
        var contents = [Float(Timer.stamp())];
        var packet = new Packet(Client(Ping), Some(contents));
        sendToServer(packet);
    }

    function bytesMessage(bytes: Bytes) {
        var packetHeader = Packet.extractPacketHeader(bytes, PacketType.Server);
        switch (packetHeader) {
            case Server(Pong):
                processServerPong(bytes);
            case _:
                trace('Received unexpected packet header: ${packetHeader}');
        }
    }

    function stringMessage(str: Dynamic) {
        var packet = str.split("|");
        var strHeader = packet[0];
        var body = packet[1];
        switch (strHeader) {
            #if debug
            case Protocol.DEBUGINFO:
                debugText.text = body;
            #end
            case _:
                trace('Received unknown string message header: ${strHeader}');
        }
    }

    /** Process Server(Pong) **/
    function processServerPong(bytes: Bytes) {
        var sentPingStamp: Float = Packet.get(bytes, Server(Pong))[0].getParameters()[0];
        var now = Timer.stamp();
        var diff = (now - sentPingStamp) * 1000; // ms
        var ping = Math.round(diff);
        pings.add(ping);
    }
}
