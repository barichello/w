package src;

#if SERVER
import src.network.Server;
#else
import src.game.Game;
#end

class Main {
    static function main() {
        #if SERVER
        new Server();
        #else
        new Game();
        #end
    }
}
